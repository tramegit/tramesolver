# TRAME SOLVER - Linear System Solver
### Versão 21.12
![](solve.png)
TRAMESOLVER it's a simple example developed in [Lazarus Pascal](https://www.lazarus-ide.org/) how to solve
systems of linear equations using matrix methods, Jacobi (iterative) and Gauss (direct).

The main aim of this program is to teach how to use dynamic arrays in object pascal language for your calculations. 

## Program features

- Size and fill out the matrix **A** and **B** to calculate the vector **X**.
- Save or load the problem data into a **mtx** file.
- Use two methods to solve the system of linear equations, one iterative(Jacobi) and one direct(Gauss).
- It's a Free and Open Source Software (FOSS).


## Examples 
![](solver0.png)
![](solver1.png)
![](solver2.png)





## License


[WTFPL](http://www.wtfpl.net/about/)

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2021 Paulo C. Ormonde < contato@trameestruturas.com.br >

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

```


