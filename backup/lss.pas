unit lss;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, math, Grids, Dialogs;

var
  n:integer;
  A: array of array of real;
  L: array of array of real;
  B: array of real;
  x: array of real;
  nint: integer;  // number of interactions


  procedure DIMATRIX (nn:integer);
  procedure JACOBI;
  procedure MGAUSS;
  procedure READMATRIX_A (grids: TStringGrid);
  procedure READMATRIX_B (grids: TStringGrid);
  procedure SHOWMATRIX_A (grids: TStringGrid;nn:integer);
  procedure SHOWMATRIX_B (grids: TStringGrid;nn:integer);
  procedure SHOWMATRIX_C (grids: TStringGrid;nn:integer);
  procedure RESULTMATRIX_C (grids: TStringGrid);
  function StrIsFloat(const S: string): boolean;


implementation



procedure DIMATRIX (nn:integer);
var i,j:integer;
begin
   SetLength(x, 0 );
   SetLength(B, 0 );
   SetLength(L, 0);
   SetLength(A, 0);

   SetLength(x, nn+1 );
   SetLength(B, nn+1 );
   SetLength(L, nn+1 );
   SetLength(A, nn+1 );

   for i := 0 to nn do begin
    SetLength(A[i], nn + 2);
    SetLength(L[i], nn + 2);
  end;


   for i := 1 to nn do begin
       B[i]:=0;
       x[i]:=0;
        for j := 1 to nn do begin
              A[i,j]:=0;
              L[i,j]:=0;
        end;
   end;

end;


// -----------------------------------------------------------------------------
//  GAUSS METHOD
//  Naïve Gaussian Elimination: assumes no diagonal element
//  is zero at any stage }
// -----------------------------------------------------------------------------
procedure MGAUSS;
var
i,j,k,n : Integer;
m,t : Real;

begin

n:=Length(B)-1;

setlength(x,n+1);

for i:= 1 to n do begin // zerando x
x[i]:=0;
end;


// Eliminate
for k:=1 to n do begin
for i:=k+1 to n do begin
m:=A[i,k]/A[k,k];
for j:=k+1 to n do A[i,j]:=A[i,j]-m*A[k,j];
B[i]:=B[i]-m*B[k];
end
end;

// BackSubstitute;
x[n]:=b[n]/a[n,n];
for k:=n-1 downto 1 do begin
t:=b[k];
for j:=k+1 to n do begin
t:=t-A[k,j]*x[j];
x[k]:=t/a[k,k];
end;
end;

end;



// -----------------------------------------------------------------------------
//  JACOBI ITERATIVE METHOD
// -----------------------------------------------------------------------------

procedure JACOBI;
var i,k,n, inter  : integer;
    soma, delta, pn, max: real;
    xt : array of real;

begin

n:=Length(B)-1;

setlength(x,n+1);
setlength(xt,n+1);

for i:= 1 to n do begin // zerando os vetores
xt[i]:=0;
x[i]:=0;
end;

pn:=0.000001;
delta:=1;

while delta > pn do begin     // pn - precisão numérica

for i:= 1 to n do begin

 // CASO D[1]
   if i=1 then begin
    soma := 0;
         for k:=2 to n do begin
         soma := soma + ( A[1,k] * x[k] );
         end;
      x[i]:= ((B[i] - soma) / A[1,1]);
    end;

// CASO D[2:nno*]
   // if ((i>1) and (i< n)) then begin
    soma := 0;
         for k:=1 to n do begin
           if k <> i then begin
            soma := soma + ( A[i,k] * x[k] );
           end;
         end;
         x[i]:=((B[i]-soma)/A[i,i]);
    // end;

// CASO x[n]
    if i = n then begin
    soma := 0;
         for k:=1 to n-1 do begin
         soma := soma + ( A[i,k] * x[k] );
         end;
       x[i]:=((B[i]-soma)/A[i,i]);
     end;

end; //for

max:=0;
for i:= 1 to n do begin // delta
  if abs(x[i] - xt[i]) >  max then max:= abs(x[i] - xt[i]);
end;
delta:=max;

    for i:= 1 to n do begin // Update Xt
     xt[i]:= x[i];
    end;
    showmessage(FloatTostr(delta));
end;   //while

  end;


// -----------------------------------------------------------------------------
//   READMATRIX_A
// -----------------------------------------------------------------------------
procedure READMATRIX_A (grids: TStringGrid);
var i,j,n:integer;
begin

n:=Length(B)-1;
if n >= 2 then begin

for i:= 1 to n do begin
    for j:= 1 to n do begin
     if grids.cells[i,j] <> '' then begin
       if StrIsFloat(grids.cells[j,i]) = true then begin
         A[i,j]:= StrToFloat (grids.cells[j,i]);
       end else begin
       A[i,j]:= 0;
       end;
   end;
end;

end;
end;
end;

// -----------------------------------------------------------------------------
//   READMATRIX_B
// -----------------------------------------------------------------------------
procedure READMATRIX_B (grids: TStringGrid);
var i,n:integer;
begin

n:=Length(B)-1;

if n >= 2 then begin

for i:= 1 to n do begin
       if grids.cells[0,i] <> '' then begin
          if StrIsFloat(grids.cells[0,i]) = true then begin
            B[i]:= StrToFloat (grids.cells[0,i]);
             end
             else begin
             B[i]:= 0;
            end;
      end;
    end;
   end;
end;



// -----------------------------------------------------------------------------
//   SHOWMATRIX_A
// -----------------------------------------------------------------------------
procedure SHOWMATRIX_A (grids: TStringGrid ; nn:integer);
var i,j:integer;
begin

grids.FixedCols:=1;
grids.FixedRows:=1;
grids.RowCount:= nn+1;
grids.ColCount:= nn+1;

for i:= 1 to nn do begin
    for j:= 1 to nn do begin
       grids.cells[i,0]:= IntToStr(i);
       grids.cells[0,j]:= IntToStr(j);
       if FormatFloat('0.0000', A[i,j]) <> '' then begin
       grids.cells[j,i]:= FormatFloat('0.0000',A[i,j]);
       end;
end;

end;

end;

// -----------------------------------------------------------------------------
//   SHOWMATRIX_B
// -----------------------------------------------------------------------------
procedure SHOWMATRIX_B (grids: TStringGrid ; nn:integer);
var i:integer;
begin
grids.FixedRows:=1;
grids.RowCount:= nn+1;
grids.cells[0,0]:= 'Values';

   for i:= 1 to nn do begin
          if FormatFloat('0.0000',B[i]) <> '' then begin
           grids.cells[0,i]:= FormatFloat('0.0000', B[i]);
          end;
   end;
end;


// -----------------------------------------------------------------------------
//   SHOWMATRIX_C
// -----------------------------------------------------------------------------
procedure SHOWMATRIX_C (grids: TStringGrid ; nn:integer);
var i:integer;
begin
  grids.FixedRows:=1;
  grids.RowCount:= nn+1;
  grids.cells[0,0]:= 'Values';
end;

// -----------------------------------------------------------------------------
//   RESULTMATRIX_C
// -----------------------------------------------------------------------------
procedure RESULTMATRIX_C (grids: TStringGrid);
var i,nn:integer;
begin
nn:=Length(B)-1;
grids.FixedRows:=1;
grids.RowCount:= nn+1;
grids.cells[0,0]:= 'Values';

   for i:= 1 to nn do begin
       grids.cells[0,i]:= FormatFloat('0.0000',x[i]);
   end;
end;

// -----------------------------------------------------------------------------
//   StrIsFloat If a string can be convert to Float value
// -----------------------------------------------------------------------------
function StrIsFloat(const S: string): boolean;
begin
  try
  StrToFloat(S);
  Result := true;
  except
  Result := false;
  end;
end;



end.

