unit gauss;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls, ComCtrls, ExtCtrls, Spin, lss, help;

type

  { TForm1 }

  TForm1 = class(TForm)
    bload: TButton;
    bsolve: TButton;
    Button2: TButton;
    Button3: TButton;
    bsave: TButton;
    b_apply: TButton;
    lb: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    ma: TStringGrid;
    mb: TStringGrid;
    mx: TStringGrid;
    mload: TOpenDialog;
    order: TSpinEdit;
    msave: TSaveDialog;
    rd: TRadioGroup;
    procedure bloadClick(Sender: TObject);
    procedure bsaveClick(Sender: TObject);
    procedure bsolveClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure b_applyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);


  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  n:integer;

implementation

{$R *.lfm}

{ TForm1 }


procedure TForm1.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.Button3Click(Sender: TObject);
var msg:string;
begin
form2.Show;
end;

procedure TForm1.b_applyClick(Sender: TObject);
begin

n:=order.Value;
DIMATRIX(n);
  if n >= 2 then begin
  lb.Caption:='Square matrix size: ' + IntToStr(n) + ' x ' + IntToStr(n);
  ma.ColCount:= n+1;
  ma.RowCount:= n+1;
  mb.RowCount:= n+1;
  mx.RowCount:= n+1;
    SHOWMATRIX_A(ma,n);
    SHOWMATRIX_B(mb,n);
    SHOWMATRIX_C(mx,n);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  decimalseparator:='.';
  n:=2;
  order.Value:=n;
  lb.Caption:='Square matrix size: ' + IntToStr(n) + ' x ' + IntToStr(n);
  ma.ColCount:= n+1;
  ma.RowCount:= n+1;
  mb.RowCount:= n+1;
  mx.RowCount:= n+1;
end;

//------------------------------------------------------------------------------
// SOLVE SYSTEM
//------------------------------------------------------------------------------

procedure TForm1.bsolveClick(Sender: TObject);
var i:integer;
begin
  DIMATRIX(n);
  READMATRIX_A(ma);
    READMATRIX_B(mb);
     n:=Length(B)-1;
      if rd.ItemIndex = 0 then MGAUSS;
      if rd.ItemIndex = 1 then JACOBI;
    RESULTMATRIX_C(mx);
end;

//------------------------------------------------------------------------------
// SAVE MATRIX
//------------------------------------------------------------------------------
procedure TForm1.bsaveClick(Sender: TObject);
var i,j:integer;
     F: Textfile;
     matrixfile:string;
begin

if Length(A)-1 >= 2 then begin

  READMATRIX_A(ma);
  READMATRIX_B(mb);

  msave.Title := 'Save Matrix';
  msave.InitialDir := application.GetNamePath;
  msave.DefaultExt:= '.mtx';

  if msave.Execute then begin
    matrixfile:= msave.FileName;
    AssignFile(F, msave.FileName);
    try
    Rewrite(F);
//--------------------------------- The writing starting
    // writing order
    writeln(F, IntToStr(n));
    // writing matrix A
    for i:= 1 to n do begin
        for j:= 1 to n do begin
         writeln(F, FloatToStr(A[i,j]));
        end;
    end;
    for i:= 1 to n do begin
      writeln(F, FloatToStr(B[i]));
    end;
    closefile(F);
//--------------------------------- Finalizing writing
      except
      on EInOutError do
        MessageDlg('File I/O error.', mtError, [mbOk], 0);
    end;
   end;
end else begin
      MessageDlg('Define the matrix!', mtError, [mbOk], 0);
  end;
end;


//------------------------------------------------------------------------------
// LOAD MATRIX
//------------------------------------------------------------------------------

procedure TForm1.bloadClick(Sender: TObject);
var i,j:integer;
     F: Textfile;
     matrixfile:string;
     txt : string;
begin

  mload.Title := 'Load Matrix';
  mload.InitialDir := application.GetNamePath;
  mload.DefaultExt:= '.mtx';

  if mload.Execute then begin
    matrixfile:= mload.FileName;
    AssignFile(F, matrixfile);
    try
    Reset(F);
//--------------------------------- The read starting
    // read order
    readln(F,txt);
    n := Strtoint(txt);
    order.Value:=n;
    DIMATRIX(n);
    // read matrix A
    for i:= 1 to n do begin
        for j:= 1 to n do begin
         readln(F,txt);
         A[i,j]:= StrtoFloat(txt);
        end;
    end;
    for i:= 1 to n do begin
      readln(F,txt);
      B[i]:=StrtoFloat(txt);
    end;
    closefile(F);
//--------------------------------- Finalizing reading
    if n >= 2 then begin
    lb.Caption:='Square matrix size: ' + IntToStr(n) + ' x ' + IntToStr(n);
    ma.ColCount:= n+1;
    ma.RowCount:= n+1;
    mb.RowCount:= n+1;
    mb.ColCount:= 1;
    mx.RowCount:= n+1;
      SHOWMATRIX_A(ma,n);
      SHOWMATRIX_B(mb,n);
    //  SHOWMATRIX_C(mx,n);
    end;
//---------------------------------- Show result
      except
      on EInOutError do
        MessageDlg('File I/O error.', mtError, [mbOk], 0);
   end;
end else begin
      MessageDlg('Define the matrix!', mtError, [mbOk], 0);
  end;
end;








end.

